package util;

import java.io.FileInputStream;
import java.util.Properties;

public class Config {
    private static Properties defaultProps = new Properties();

    static {
        try {
            FileInputStream in = new FileInputStream("src/main/resources/config.properties");
            defaultProps.load(in);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String get(String key) {
        return defaultProps.getProperty(key);
    }

    public static String get(String key, String defaultValue) {
        String property = get(key);

        if(property.isEmpty()){
            return defaultValue;
        }

        return property;
    }
}