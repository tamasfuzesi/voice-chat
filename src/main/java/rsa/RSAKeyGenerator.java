package rsa;

import util.Config;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

public class RSAKeyGenerator {
    private RSAPublicKey publicKey;
    private RSAPrivateKey privateKey;

    private static int keySize = Integer.parseInt(Config.get("rsa.keysize", "2048"));

    public RSAKeyGenerator(KeyPair keyPair) {
        publicKey = new RSAPublicKey(keyPair.getPublic().getEncoded());
        privateKey = new RSAPrivateKey(keyPair.getPublic().getEncoded());
    }

    public RSAPrivateKey getPrivateKey() {
        return privateKey;
    }

    public RSAPublicKey getPublicKey() {
        return publicKey;
    }

    public static RSAKeyGenerator generate() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(RSAKeyGenerator.keySize);
        KeyPair keyPair = keyGen.generateKeyPair();

        return new RSAKeyGenerator(keyPair);
    }
}
