package network;

public class NetworkFactory {
    private Receiver receiver;
    private Sender sender;

    public NetworkFactory(String networkType) {
        if(networkType.isEmpty()){
            throw new IllegalArgumentException("Network type is not valid.");
        }

        if(networkType.equals("tcp")){
            receiver = new TCPServer();
            sender = new TCPClient();
        } else if(networkType.equals("udp")){
            receiver = new UDPServer();
            sender = new UDPClient();
        } else {
            throw new RuntimeException("Invalid network type.");
        }
    }

    public Receiver getServer() {
        return receiver;
    }

    public Sender getClient(){
        return sender;
    }
}
