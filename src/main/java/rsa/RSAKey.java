package rsa;

import java.util.Base64;

abstract public class RSAKey {
    private byte[] key;

    public void setKey(byte[] key) {
        this.key = key;
    }

    public byte[] getKey() {
        return key;
    }

    public String toBase64String() {
        return Base64.getEncoder().encodeToString(getKey());
    }

    public RSAKey fromBase64String(String base64Encoded) {
        setKey(Base64.getDecoder().decode(base64Encoded));
        return this;
    }
}
